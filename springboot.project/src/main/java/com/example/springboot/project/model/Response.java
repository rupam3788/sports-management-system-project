package com.example.springboot.project.model;

public class Response {
	int status;
	PlayerLoginDetails input;
	String reason;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Response() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PlayerLoginDetails getInput() {
		return input;
	}
	public void setInput(PlayerLoginDetails input) {
		this.input = input;
	}
	public Response(int status, PlayerLoginDetails input, String reason) {
		super();
		this.status = status;
		this.input = input;
		this.reason = reason;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}
