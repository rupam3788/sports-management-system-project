package com.example.springboot.project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.springboot.project.model.PlayerLoginDetails;

@Repository
public interface PlayerLoginRespository extends JpaRepository<PlayerLoginDetails, Integer>{
		
	@Query("from PlayerLoginDetails where email=:x and password=:y")
	PlayerLoginDetails findByIdAndPin(@Param("x") String email,@Param("y") String password);
}
