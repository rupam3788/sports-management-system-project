package com.example.springboot.project.service;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.springboot.project.dao.ManagerLoginRepository;
import com.example.springboot.project.dao.ManagerRepository;
import com.example.springboot.project.dao.PlayerLoginRespository;
import com.example.springboot.project.dao.PlayerRepository;
import com.example.springboot.project.exception.ResourceNotFoundException;
import com.example.springboot.project.model.PlayerLoginDetails;
import com.example.springboot.project.model.Events;
import com.example.springboot.project.model.MResponse;
import com.example.springboot.project.model.ManagerLoginDetails;
import com.example.springboot.project.model.PlayerDetails;
import com.example.springboot.project.model.Response;

@Service
public class ServiceImplementation implements ServiceInterface {
	
	@Autowired
	PlayerLoginRespository sr;
	
	@Autowired
	PlayerRepository pr;
	
	@Autowired
	ManagerLoginRepository mr;
	
	@Autowired
	ManagerRepository me;

	@Override
	public Response registerDetails(PlayerLoginDetails input) {
		Response r = new Response(0,input,"failed");
		
		input.setLogin_person("Player");
		try {
			sr.save(input);
			r.setReason("Successful");
			r.setStatus(1);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("jpa error");
		}
		
		return r;
	}

	@Override
	public Response checkLoginDetails(String email, String password) {
		Response r = new Response(0,null,"failed");
		PlayerLoginDetails ld=sr.findByIdAndPin(email,password);
		System.out.println(ld);
		System.out.println("Hello");
		if(ld!=null) {
			r.setStatus(1);
			r.setReason("Successful");
			r.setInput(ld);
		}
		else
			System.out.println("not found");
		return r;
	}

	@Override
	public ResponseEntity<PlayerDetails> playerDetailsReg(int id, PlayerDetails input) {
		PlayerLoginDetails pdetails = sr.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Player not exist with id :" + id));
		PlayerDetails addDetails = null;
		
//		pdetails.setPlayer_id(input);
		if(pdetails.getPlayer_id()==null) {
		input.setLogin_details(pdetails);
		addDetails = pr.save(input);
		
		}
		return ResponseEntity.ok(addDetails);
	}

	@Override
	public MResponse checkLoginDetailsM(String email, String password) {
		MResponse r = new MResponse(0,null,"failed");
		ManagerLoginDetails mld=mr.findByIdAndPin(email,password);
		if(mld!=null) {
			r.setStatus(1);
			r.setReason("Successful");
			r.setDetails(mld);
		}
		else
			System.out.println("not found");
		return r;
	}

//	@Override
//	public String addEvent(Events event) {
//		String s="unsuccessful";
//		try {
//			String name = event.getEventname();
//			Date dt = event.getDate();
//			Time t = event.getTime();
//			me.saveByManId(name,dt,t);
//			s="successful";
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			System.out.println("jpa error");
//		}
//		return s;
//	}

	@Override
	public ResponseEntity<ManagerLoginDetails> addEvent(int id, Events event) {
		ManagerLoginDetails mdetails = mr.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Manager not exist with id :" + id));
		
		//List<Events> e = event.getEvents();
		
		mdetails.addEvent(event);
		me.save(event);
		ManagerLoginDetails addEvent = mr.save(mdetails);
		return ResponseEntity.ok(addEvent);
	}

	@Override
	public List<Events> getAllEvents() {
		List<Events> l=me.findAll();
		return l;
	}

	@Override
	public ResponseEntity<Map<String, Boolean>> findEventById(int id) {
		Events event = me.findById(id)
		.orElseThrow(() -> new ResourceNotFoundException("Event not exist with id :" + id));
		me.delete(event);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
		
	}

	@Override
	public ResponseEntity<Events> findEventByIdAndUpdate(int id, Events eventDetails) {
		Events event = me.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Event not exist with id :" + id));
		
		event.setEventname(eventDetails.getEventname());
		event.setDate(eventDetails.getDate());
		event.setTime(eventDetails.getTime());
		
		Events updatedEvent = me.save(event);
		return ResponseEntity.ok(updatedEvent);
	}

	@Override
	public List<PlayerLoginDetails> getAllPlayers() {
		return sr.findAll();
		 
	}

	@Override
	public ResponseEntity<PlayerDetails> takePartInEvent(int pid, int eid) {
		PlayerDetails pd = pr.findById(pid)
				.orElseThrow(() -> new ResourceNotFoundException("Player not exist with id :" + pid));
		Events e = me.findById(eid)
				.orElseThrow(() -> new ResourceNotFoundException("Event not exist with id :" + eid));
			
		
		e.addPlayer(pd);

		
		PlayerDetails eventpart = pr.save(pd);
		return ResponseEntity.ok(eventpart);
	}

	@Override
	public List<Events> eventsTakenPartSer(int id) {
		PlayerDetails pd = pr.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Player not exist with id :" + id));
		

		List<Events> el = pd.getEvents();
		
		
		return el;
	}

	@Override
	public ResponseEntity<PlayerLoginDetails> getPersonalDetailsSer(int id) {
		PlayerLoginDetails pdetails = sr.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Player not exist with id :" + id));
		
		
		return ResponseEntity.ok(pdetails);
	}

	@Override
	public ResponseEntity<Events> getEventId(int id) {
		Events eid =me.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Event not exist with id :" + id));
		
		return ResponseEntity.ok(eid);
	}

	@Override
	public ResponseEntity<ManagerLoginDetails> getManagerIdSer(int id) {
		ManagerLoginDetails mld =mr.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Manager not exist with id :" + id));
		
		return ResponseEntity.ok(mld);
	}

	@Override
	public ResponseEntity<PlayerDetails> getPlayerIdSer(int id) {
		PlayerDetails pld =pr.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Player not exist with id :" + id));
//		System.out.println(pld);
//		System.out.println("Hello");
		return ResponseEntity.ok(pld);
	}

	@Override
	public ResponseEntity<PlayerDetails> findPlayerByIdAndUpdate(int id, PlayerDetails pd) {
		PlayerDetails p = pr.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Player not exist with id :" + id));
		
		p.setFathername(pd.getFathername());
		p.setDob(pd.getDob());
		p.setGender(pd.getGender());
		p.setAddress(pd.getAddress());
		p.setCollegename(pd.getCollegename());
		p.setPhone(pd.getPhone());
		
		PlayerDetails updateplayer = pr.save(p);
		return ResponseEntity.ok(updateplayer);
	}

//	@Override
//	public ResponseEntity<Map<String, Boolean>> findplayerById(int id) {
//		Player event = me.findById(id)
//				.orElseThrow(() -> new ResourceNotFoundException("Event not exist with id :" + id));
//				me.delete(event);
//				Map<String, Boolean> response = new HashMap<>();
//				response.put("deleted", Boolean.TRUE);
//				return ResponseEntity.ok(response);
//		return null;
//	}

}
