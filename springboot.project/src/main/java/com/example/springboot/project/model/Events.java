package com.example.springboot.project.model;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name = "events")
public class Events {
	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private int id;
	
	private String eventname;
	private Date date;
	private Time time;
	
//	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "event_1")
//	private PlayerDetails pd1;
//	
//	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "event_2")
//	private PlayerDetails pd2;
//	
//	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "event_3")
//	private PlayerDetails pd3;
//	
//	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "event_4")
//	private PlayerDetails pd4;
	
	@JsonIgnore
	@ManyToMany(mappedBy="events")
    private List<PlayerDetails> players;
	
	public void addPlayer(PlayerDetails pp) {
	    players.add(pp);
	    pp.getEvents().add(this);
	    }

	    public void removePlayer(PlayerDetails pp) {
	        players.remove(pp);
	        pp.getEvents().remove(this);
	    }
	
	
	   
	    @JsonIgnore
	    @ManyToOne(fetch= FetchType.LAZY)
	    private ManagerLoginDetails m;


	
	
	
	
	public ManagerLoginDetails getM() {
			return m;
		}

		public void setM(ManagerLoginDetails m) {
			this.m = m;
		}

	public List<PlayerDetails> getPlayers() {
			return players;
		}

		public void setPlayers(List<PlayerDetails> players) {
			this.players = players;
		}

	public Events() {
		
	}
	
	
	public Events(String eventname, Date date, Time time) {
		super();
		this.eventname = eventname;
		this.date = date;
		this.time = time;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEventname() {
		return eventname;
	}
	public void setEventname(String eventname) {
		this.eventname = eventname;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}
	
	
}