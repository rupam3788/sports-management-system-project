package com.example.springboot.project.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "managerlogindetails")
public class ManagerLoginDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "first_name")
	private String first_name;
	@Column(name = "last_name")
	private String last_name;
	@Column(name = "email_Id")
	private String email;
	@Column(name = "password")
	private String password;
	@Column(name = "login_person")
	private String login_person;
	
	 @OneToMany(mappedBy="m",
     orphanRemoval = true)
	private List<Events> events;
	
	public void addEvent(Events pp) {
	    events.add(pp);
	    pp.setM(this);
	    
	    }

	    public void removeEvent(Events pp) {
	        events.remove(pp);
	        pp.setM(null);
	    }
	
	public ManagerLoginDetails() {
		super();
		
	}



	public ManagerLoginDetails(String first_name, String last_name, String email, String password,
			String login_person) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.password = password;
		this.login_person = login_person;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getFirst_name() {
		return first_name;
	}



	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}



	public String getLast_name() {
		return last_name;
	}



	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getLogin_person() {
		return login_person;
	}



	public void setLogin_person(String login_person) {
		this.login_person = login_person;
	}



	public List<Events> getEvents() {
		return events;
	}



	public void setEvents(List<Events> events) {
		this.events = events;
	}
	
	
}

