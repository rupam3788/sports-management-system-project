package com.example.springboot.project.service;

import com.example.springboot.project.model.PlayerLoginDetails;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.example.springboot.project.model.Events;
import com.example.springboot.project.model.MResponse;
import com.example.springboot.project.model.ManagerLoginDetails;
import com.example.springboot.project.model.PlayerDetails;
import com.example.springboot.project.model.Response;

public interface ServiceInterface {
	public Response registerDetails(PlayerLoginDetails input);
	public Response checkLoginDetails(String email, String password);
	public ResponseEntity<PlayerDetails> playerDetailsReg(int id,PlayerDetails input);
	public MResponse checkLoginDetailsM(String email, String password);
	public ResponseEntity<ManagerLoginDetails> addEvent(int id,Events event);
//	public String addEvent(Events event);
	public List<Events> getAllEvents();
	public ResponseEntity<Map<String, Boolean>> findEventById(int id);
	public ResponseEntity<Events> findEventByIdAndUpdate(int id, Events eventDetails);
	public List<PlayerLoginDetails> getAllPlayers();
	public ResponseEntity<PlayerDetails> takePartInEvent(int pid,int eid);
	public List<Events> eventsTakenPartSer(int id);
	public ResponseEntity<PlayerLoginDetails> getPersonalDetailsSer(int id);
	public ResponseEntity<Events> getEventId(int id);
	public ResponseEntity<ManagerLoginDetails> getManagerIdSer(int id);
	public ResponseEntity<PlayerDetails> getPlayerIdSer(int id);
	//public ResponseEntity<Map<String, Boolean>> findplayerById(int id);
	public ResponseEntity<PlayerDetails> findPlayerByIdAndUpdate(int id, PlayerDetails pd);
}
