package com.example.springboot.project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.springboot.project.model.PlayerDetails;
@Repository
public interface PlayerRepository extends JpaRepository<PlayerDetails, Integer>{

}
