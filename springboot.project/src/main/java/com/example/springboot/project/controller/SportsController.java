package com.example.springboot.project.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.project.model.PlayerLoginDetails;
import com.example.springboot.project.model.Events;
import com.example.springboot.project.model.MResponse;
import com.example.springboot.project.model.ManagerLoginDetails;
import com.example.springboot.project.model.PlayerDetails;
import com.example.springboot.project.model.Response;
import com.example.springboot.project.service.ServiceInterface;




@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class SportsController {
	
	@Autowired
	private ServiceInterface si;
	
	
	@PostMapping("/register")
	public Response RegisterDetails(@RequestBody PlayerLoginDetails input) {
       
		return si.registerDetails(input);
		}
 	
	@GetMapping("/login/{email}/{password}")
    public Response LoginDetails(@PathVariable("email") String email,@PathVariable("password") String password)
    {
 		return si.checkLoginDetails(email, password);
    }
 	
 	@GetMapping("/mlogin/{email}/{password}")
    public MResponse MLoginDetails(@PathVariable("email") String email,@PathVariable("password") String password)
    {
 		return si.checkLoginDetailsM(email, password);
    }
 	
 	@PostMapping("/registerplayerdetails/{id}")
 	public ResponseEntity<PlayerDetails> RegisterPlayerDetails(@PathVariable int id, @RequestBody PlayerDetails input) {
 		return si.playerDetailsReg(id,input);
 	}
 	

 	@PostMapping("/event/{id}")
 	public ResponseEntity<ManagerLoginDetails> addNewEvent(@PathVariable int id,@RequestBody Events event) {
 		
 		System.out.println("hi");
 		return si.addEvent(id,event);
 	}
 	
 	@GetMapping("/event")
 	public List<Events> getAllEvents(){
 		return si.getAllEvents();
 	}
 	
 	@DeleteMapping("/event/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteEvent(@PathVariable int id){
		return si.findEventById(id);
	}
 	
// 	@DeleteMapping("/removeplayer/{id}")
// 	public ResponseEntity<Map<String, Boolean>> removeplayer(@PathVariable int id){
//		return si.findplayerById(id);
//	} 
 	
 	@PutMapping("/event/{id}")
	public ResponseEntity<Events> updateEvent(@PathVariable int id, @RequestBody Events eventDetails){
		return si.findEventByIdAndUpdate(id,eventDetails);
	}
 	
 	@PutMapping("/updateplayer/{id}")
 	public ResponseEntity<PlayerDetails> updatePlayer(@PathVariable int id,@RequestBody PlayerDetails pd){
 		return si.findPlayerByIdAndUpdate(id, pd);
 	}
 	
 	@GetMapping("/allplayers")
 	public List<PlayerLoginDetails> getAllPlayers(){
 		return si.getAllPlayers();
 	}
 	
 	@PutMapping("/takepartevent/{pid}/{eid}")
 	public ResponseEntity<PlayerDetails> takePart(@PathVariable("pid") int pid, @PathVariable("eid") int eid){
 		return si.takePartInEvent(pid,eid);
 	}
 	
 	@GetMapping("/eventstakenpart/{id}")
 	public List<Events> eventsTakenPart(@PathVariable int id){
 		return si.eventsTakenPartSer(id);
 	}
 	
 	
 	@GetMapping("/getpersonaldetails/{id}")
 	public ResponseEntity<PlayerLoginDetails> getPersonalDetails(@PathVariable int id) {
 		return si.getPersonalDetailsSer(id);
 	}
 	
 	@GetMapping("/event/{id}")
 	public ResponseEntity<Events> getEventId(@PathVariable int id) {
 		return si.getEventId(id);
 	}
 	
 	
 	@GetMapping("/managerid/{id}")
 	public ResponseEntity<ManagerLoginDetails> getManagerId(@PathVariable int id){
 		return si.getManagerIdSer(id);
 	}
 	
 	@GetMapping("/playerid/{id}")
 	public ResponseEntity<PlayerDetails> getPlayerId(@PathVariable int id){
 		return si.getPlayerIdSer(id);
 	}
 	
// 	@PostMapping("/add_event")
// 	public String addNewEvent(@pathVariable int id, @RequestBody Events event) {
// 		return si.addEvent(event);
// 	}
 	
 	
}
