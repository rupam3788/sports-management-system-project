package com.example.springboot.project.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "player_details")
public class PlayerDetails {
	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private int id;
	@Column(name = "fathers_name")
	private String fathername;
	@Column(name = "DOB")
	private LocalDate dob;
	@Enumerated(EnumType.STRING)
	@Column(name = "gender")
	private Gender gender;
	@Column(name = "address")
	private String address;
	@Column(name = "phoneNumber")
	private String phone;
	@Column(name = "college_name")
	private String collegename;
//	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	@JoinColumn(name = "event_1", referencedColumnName = "id")
//	private Events event_1;
//	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	@JoinColumn(name = "event_2", referencedColumnName = "id")
//	private Events event_2;
//	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	@JoinColumn(name = "event_3", referencedColumnName = "id")
//	private Events event_3;
//	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	@JoinColumn(name = "event_4", referencedColumnName = "id")
//	private Events event_4;
	
	
	@ManyToMany(fetch= FetchType.LAZY)
    private List<Events> events;
	
	
	
	
	public List<Events> getEvents() {
		return events;
	}


	public void setEvents(List<Events> events) {
		this.events = events;
	}
	//	@OneToOne
//    @PrimaryKeyJoinColumn(name = "id")
//    private Events event_1;
//	@OneToOne
//	@PrimaryKeyJoinColumn(name = "id")
//    private Events event_2;
//	@OneToOne
//	@PrimaryKeyJoinColumn(name = "id")
//    private Events event_3;
//	@OneToOne
//	@PrimaryKeyJoinColumn(name = "id")
//    private Events event_4;
	@OneToOne( cascade = CascadeType.ALL)
	@JoinColumn(name = "login_details", referencedColumnName = "id")
	private PlayerLoginDetails login_details;
	
	
	
	
	
	
	
//	@Override
//	public String toString() {
//		return "PlayerDetails [id=" + id + ", fathername=" + fathername + ", dob=" + dob + ", gender=" + gender
//				+ ", address=" + address + ", phone=" + phone + ", collegename=" + collegename + ", events=" + events
//				+ ", login_details=" + login_details + "]";
//	}


	public PlayerDetails() {
		super();
	}
	
	
	public PlayerDetails(String fathername, LocalDate dOB, Gender gender, String address, String phone,
			String collegename) {
		super();
		this.fathername = fathername;
		dob = dOB;
		this.gender = gender;
		this.address = address;
		this.phone = phone;
		this.collegename = collegename;
	}
	public String getFathername() {
		return fathername;
	}
	public void setFathername(String fathername) {
		this.fathername = fathername;
	}
	public LocalDate getDob() {
		return dob;
	}
	public void setDob(LocalDate dOB) {
		dob = dOB;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCollegename() {
		return collegename;
	}
	public void setCollegename(String collegename) {
		this.collegename = collegename;
	}


	public PlayerLoginDetails getLogin_details() {
		return login_details;
	}


	public void setLogin_details(PlayerLoginDetails login_details) {
		this.login_details = login_details;
	}

	
}
