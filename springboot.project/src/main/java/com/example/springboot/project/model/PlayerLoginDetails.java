package com.example.springboot.project.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "playerlogindetails")
public class PlayerLoginDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "first_name")
	private String first_name;
	@Column(name = "last_name")
	private String last_name;
	@Column(name = "email_Id")
	private String email;
	@Column(name = "password")
	private String password;
	@Column(name = "login_person")
	private String login_person;
	@JsonIgnore
	@OneToOne(mappedBy = "login_details", orphanRemoval = true)
	private PlayerDetails player_id;
//	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@JoinColumn(name = "pe_id")
//	private PlayerEvents pe_id;
	
	public PlayerLoginDetails() {
		super();
	}
	
	public PlayerLoginDetails(String first_name, String last_name, String email, String password,String login_person,PlayerDetails player_id) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.password = password;
		this.login_person = login_person;
		this.player_id = player_id;
	}
	
	
//	public PlayerEvents getPe_id() {
//		return pe_id;
//	}
//
//	public void setPe_id(PlayerEvents pe_id) {
//		this.pe_id = pe_id;
//	}

	public PlayerDetails getPlayer_id() {
		return player_id;
	}
	public void setPlayer_id(PlayerDetails player_id) {
		this.player_id = player_id;
	}
	public String getLogin_person() {
		return login_person;
	}
	public void setLogin_person(String login_person) {
		this.login_person = login_person;
	}
	public String getFirst_name() {
		return first_name;
	}
	
//	@Override
//	public String toString() {
//		return "PlayerLoginDetails [id=" + id + ", first_name=" + first_name + ", last_name=" + last_name + ", email="
//				+ email + ", password=" + password + ", login_person=" + login_person + ", player_id=" + player_id
//				+ "]";
//	}

	public void setFirstname(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setLastname(String last_name) {
		this.last_name = last_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
