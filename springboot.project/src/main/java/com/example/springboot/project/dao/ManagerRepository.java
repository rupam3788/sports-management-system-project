package com.example.springboot.project.dao;

import java.sql.Date;
import java.sql.Time;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.springboot.project.model.Events;

@Repository
public interface ManagerRepository extends JpaRepository<Events, Integer>{
//	@Query("insert into managerdetails e (e.eventname,e.date,e.time) values(:x,:y,:z) where id=:w")
//	@Transactional
//	Events saveByManId(@Param("x")String name,@Param("y") Date dt,@Param("z") Time t);
	
}
