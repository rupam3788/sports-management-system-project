package com.example.springboot.project.model;

public class MResponse {
	int status;
	ManagerLoginDetails input;
	String reason;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public MResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ManagerLoginDetails getInput() {
		return input;
	}
	public void setDetails(ManagerLoginDetails input) {
		this.input = input;
	}
	public MResponse(int status, ManagerLoginDetails input, String reason) {
		super();
		this.status = status;
		this.input = input;
		this.reason = reason;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}

