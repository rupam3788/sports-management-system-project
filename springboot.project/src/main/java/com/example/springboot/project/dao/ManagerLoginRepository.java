package com.example.springboot.project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.springboot.project.model.ManagerLoginDetails;

@Repository
public interface ManagerLoginRepository extends JpaRepository<ManagerLoginDetails, Integer>{
	
	@Query("from ManagerLoginDetails where email=:x and password=:y")
	ManagerLoginDetails findByIdAndPin(@Param("x") String email,@Param("y") String password);
}
