import React, { Component } from 'react';

class FooterFront extends Component {
    render() {
        return (
            <div>
            <nav class="navbar navbar-expand-md bg-dark navbar-dark justify-content-center">
                <h3 className="text-center">“The only way to prove that you're a good sport is to lose.”</h3>
                </nav>
                
            </div>
        );
    }
}

export default FooterFront;