import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Login from "./components/login.component";
import Playerlogin from "./components/Playerlogin";
import SignUp from "./components/signup.component";
import AfterLoginAdmin from './components/AfterLoginAdmin';
import CreateEventListByAdmin from'./components/CreateEventListByAdmin';
import UpdateEventByAdmin from'./components/UpdateEventByAdmin';
import ViewEventListByAdmin from './components/ViewEventListByAdmin';
import PlayerDetailsSeenByAdmin from './components/PlayerDetailsSeenByAdmin';
import Query from './components/Query';
import ViewPlayerListByAdmin from './components/ViewPlayerListByAdmin';
import AfterLoginPlayer from './components/AfterLoginPlayer';
import DetailsFilledAfterLoginPlayer from './components/DetailsFilledAfterLoginPlayer';
import Profile from './components/Profile';
import ProfileEvent from './components/ProfileEvent';
import Front from './Front';
import UpdatePlayerDetails from './components/UpdatePlayerDetails';

function App() {
  return (<Router>
   
    
    

      
          <Switch>
          <Route exact path='/' component={Front} />
          <Route  path='/home' component={Front} />
            {/* <Route exact path='/' component={Login} /> */}
            <Route path="/sign-in" component={Login} />
            <Route path="/psign-in" component={Playerlogin} />
            <Route path="/sign-up" component={SignUp} />
            <Route path="/event" component={AfterLoginAdmin} />
            <Route path = "/add-event/:id" component = {CreateEventListByAdmin}></Route>
            <Route path = "/update-event/:id" component = {UpdateEventByAdmin}></Route>
            <Route path = "/view-event/:id" component = {ViewEventListByAdmin}></Route>
            <Route path="/player" component={PlayerDetailsSeenByAdmin} />
            <Route path = "/view-player/:id" component = {ViewPlayerListByAdmin}></Route>
            <Route path="/afterloginplayerfilldetails" component={DetailsFilledAfterLoginPlayer} />
            <Route path="/afterloginplayer" component={AfterLoginPlayer} />
            <Route path="/profile" component={Profile} />
            <Route path="/update-player/:playerId" component={UpdatePlayerDetails} />
            <Route path="/profileevent" component={ProfileEvent} />
            <Route path="/query" component={Query} />
          </Switch>
      
    
    </Router>
  );
}

export default App;
