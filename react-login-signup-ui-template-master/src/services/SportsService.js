import axios from 'axios';

const EVENT_API_BASE_URL = "http://localhost:7777/api/v1/event";
const PLAYER_API_BASE_URL = "http://localhost:7777/api/v1/allplayers";
const PID_API_BASE_URL = "http://localhost:7777/api/v1/playerid";
const UPID_API_BASE_URL = "http://localhost:7777/api/v1/updateplayer";
const DETAILS_API_BASE_URL = "http://localhost:7777/api/v1/details";
const manager_id = "http://localhost:7777/api/v1/managerid";


class SportsService {

    //  registerPlayer(input){
    //      return axios.post(PLAYER_API_BASE_URL,input);
    //  }

    //  loginPlayer(email,password){
    //      return axios.get(PLAYER1_API_BASE_URL,email,password);
    //  }

    getEvent(){
        return axios.get(EVENT_API_BASE_URL);
    }

 

    addEvent(managerid,event) {
        const url = `${EVENT_API_BASE_URL}/${managerid}`;
        const data = event;
        const headers = {
          "Content-Type": "application/json",
        };
        return axios.post(url, data, headers);
      }

    getEventById(eventId){
        return axios.get(EVENT_API_BASE_URL + '/' + eventId);
    }

    getManagerById(idm){
        return axios.get(manager_id + '/' + idm);
    }

    // getPlayerById(playerid){
    //     return axios.get(PLAYER1_API_BASE_URL + '/' + playerid);
    // }

    updateEvent(event, eventId){
        return axios.put(EVENT_API_BASE_URL + '/' + eventId, event);
    }

    deleteEvent(eventId){
        return axios.delete(EVENT_API_BASE_URL + '/' + eventId);
    }

    getPlayer(){
        return axios.get(PLAYER_API_BASE_URL);
    }
    getPlayerById(playerId){
        return axios.get(PID_API_BASE_URL + '/' + playerId);
    }

    // updatePlayer(player, playerId){
    //     return axios.put(PLAYER_API_BASE_URL + '/' + player, playerId);
    // }

    // deletePlayer(playerId){
    //     return axios.delete(PLAYER_API_BASE_URL + '/' + playerId);
    // }

    updatePlayer(playerId,input){
        return axios.put(UPID_API_BASE_URL + '/' + playerId, input);
    }

    getEventDetailsinPlayer(){
        return axios.get(EVENT_API_BASE_URL);
    }
    getPlayerDetailsById(input){
        return axios.get( PID_API_BASE_URL + '/' + input);

    }
    getEventDetailsforPlayerById(playerId){
        return axios.get(DETAILS_API_BASE_URL + '/' + playerId);

    }
    
}

 export default new SportsService()