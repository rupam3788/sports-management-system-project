import React, { Component } from 'react';
import SportsService from '../services/SportsService';
import FooterAdmin from './FooterAdmin';
import HeaderAdmin from './HeaderAdmin';

class PlayerDetailsSeenByAdmin extends Component {

    constructor(props) {
        super(props)

        this.state = {
                player: []
        }
    
        this.deleteEvent = this.deleteEvent.bind(this);


        
    }

    deleteEvent(id){
        if(localStorage.getItem("mid")!=null){
        SportsService.deletePlayer(id).then( res => {
            this.setState({player: this.state.player.filter(player => player.id !== id)});
        });
    }
    }
    viewPlayer(id){
        if(localStorage.getItem("mid")!=null){
        this.props.history.push(`/view-player/${id}`);
        }
    }

    componentDidMount(){
        if(localStorage.getItem("mid")!=null){
        SportsService.getPlayer().then((res) => {
            this.setState({ player: res.data});
            // console.log(this.state.player[0].id)
        });
    }
    }

    render() {
        return (
            <div>
                <HeaderAdmin/>
                <h2 className="text-center">All Player List</h2>
            
            <br></br>
            <div className = "row">
                   <table className = "table table-striped table-bordered">

                       <thead>
                           <tr>
                              <th className="text-center"> ID</th>
                               <th className="text-center"> First Name</th>
                               
                               <th className="text-center"> Last Name</th>
                               <th className="text-center"> Email</th>
                               {/* <th> Father's Name</th>
                               <th> Date of Birth</th>
                               <th> Gender</th>
                               <th> Address</th> */}
                               {/* <th> Phone Number</th> */}
                               <th className="text-center"> Actions</th>
                               {/* <th> College Name</th> */}
                               {/* <th> Address</th> */}
                           </tr>
                       </thead>
                       <tbody>
                           {
                               this.state.player.map(
                                   players => 
                                   <tr key = {players.id}>
                                       
                                       <td className="text-center">{players.id}</td>
                                       <td className="text-center"> { players.first_name} </td> 
                                        <td className="text-center"> { players.last_name} </td> 
                                       <td className="text-center"> { players.email} </td> 
                                        {/* <td> { player.fathername} </td>   
                                        <td> { player.dob} </td> 
                                        <td> { player.gender} </td>
                                        <td> { player.address} </td>    */}
                                        {/* <td> { players.phone} </td>  */}
                                        {/* <td> { player.collegename} </td>  */}
                                        <td className="text-center">
                                        {/* <button style={{marginLeft: "10px"}} onClick={ () => this.deleteEvent(players.id)} className="btn btn-danger">Delete </button> */}
                                        <button style={{marginLeft: "10px"}} onClick={ () => this.viewPlayer(players.id)} className="btn btn-info">View </button>
                        
                                        </td>
                                   </tr>
                               )
                           }
                       </tbody>
                   </table>

            </div>
            <FooterAdmin/>

       </div>
   );
    }
}

export default PlayerDetailsSeenByAdmin;