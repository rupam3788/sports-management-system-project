import React, { Component } from 'react';
import SportsService from '../services/SportsService';
import HeaderForAdmin from './HeaderForAdmin';
import FooterForAdmin from './FooterForAdmin';

class AfterLoginAdmin extends Component {

    state = {
        event: [],
        managerId:""
}
    // constructor(props) {
    //     super(props)

    //     this.state = {
    //             event: [],
    //             managerId:"",
    //     }

    //     this.addEvent = this.addEvent.bind(this);
    //     this.editEvent = this.editEvent.bind(this);
    //     this.deleteEvent = this.deleteEvent.bind(this);


        
    // }

    componentDidMount() {
        if(localStorage.getItem("mid")!=null){
        console.log("hi", localStorage.getItem("mid"))
        this.setState({managerId:localStorage.getItem("mid")})
        console.log(localStorage.getItem("mid"))

        SportsService.getEvent().then((res) => {
            this.setState({ event: res.data});
        });
    }
        
    }
    

    deleteEvent=(id)=>{
        
        SportsService.deleteEvent(id).then( res => {
            this.setState({event: this.state.event.filter(event => event.id !== id)});
        });
    
    }
    viewEvent=(id)=>{
      
        this.props.history.push(`/view-event/${id}`);
        
    }
    editEvent=(id)=>{
        
        this.props.history.push(`/update-event/${id}`);
    
    }
    addEvent=()=>{
        if(localStorage.getItem("mid")!=null){
        this.props.history.push(`/add-event/${this.state.managerId}`);
        }
    }
    // addEvent(id){
    //     this.props.history.push('/add-event/${id}');
    // }

    

    
    render() {
        return (
            
            <div>
                <HeaderForAdmin/>
                <div className="form-group">
                 <h2 className="text-center">EVENT LIST</h2>
                 <div className = "row">
                    <button className="btn btn-primary" onClick={this.addEvent}> Add EVENT</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th className="text-center"> Event Name</th>
                                    <th className="text-center"> Event Date</th>
                                    <th className="text-center"> Event Time</th>
                                    <th className="text-center"> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.event.map(
                                        event => 
                                        <tr key = {event.id}>
                                             <td className="text-center"> {event.eventname} </td>   
                                             <td className="text-center"> {event.date}</td>
                                             <td className="text-center"> {event.time}</td>
                                             <td className="text-center">
                                                 <button onClick={ () => this.editEvent(event.id)} className="btn btn-info">Update </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteEvent(event.id)} className="btn btn-danger">Delete </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewEvent(event.id)} className="btn btn-info">View </button>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
            <FooterForAdmin/> 
            </div>  
             
        );
    }
}

export default AfterLoginAdmin;