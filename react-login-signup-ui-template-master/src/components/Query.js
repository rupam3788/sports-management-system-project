import React, { Component } from 'react';
import HeaderQuery from './HeaderQuery';
import FooterQuery from './FooterQuery';
import SportsService from '../services/SportsService';

class Query extends Component {

    
    constructor(props){
        super(props)

        this.state = {
            input: [],
            managerId:""
    }
}

    componentDidMount() {
        console.log(localStorage.getItem("mid"))

        const idm=localStorage.getItem("mid");

        SportsService.getManagerById(1).then((res) => {
            console.log(res.data)
            this.setState({input : res.data});
        });
        
    }
    
    render() {
        return (
            
            <div>
                <HeaderQuery/>
                <div className="form-group">
                 <h2 className="text-center">Admin Contact List</h2>
                 <div className = "row">
                    {/* <button className="btn btn-primary" onClick={this.addEvent}> Add EVENT</button> */}
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th className="text-center"> First Name</th>
                                    <th className="text-center"> Last Name</th>
                                    <th className="text-center"> Email</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                                   <td className="text-center"> {this.state.input.first_name} </td>   
                                   <td className="text-center"> {this.state.input.last_name}</td>
                                   <td className="text-center"> {this.state.input.email}</td>
                                   
                               
                            </tbody>
                            {/* <tbody>
                                {
                                    this.state.input.map(
                                        input => 
                                        <tr key = {input.id}>
                                             <td> {input.first_name} </td>   
                                             <td> {input.last_name}</td>
                                             <td> {input.email}</td>
                                             
                                        </tr>
                                    )
                                }
                            </tbody> */}
                        </table>

                 </div>

            </div>
            <FooterQuery/> 
            </div>  
             
        );
    }
}
export default Query;