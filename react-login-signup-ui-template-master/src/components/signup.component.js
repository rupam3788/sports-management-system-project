import React, { Component } from "react";
//import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
//import SportsService from "../services/SportsService";
import {  Link } from "react-router-dom";
 import axios from 'axios';
 //import './App.css';
 import {backendUrl} from './constants';

export default class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
          
          input: {},
          errors: {}
        };
         
        this.handleChange = this.handleChange.bind(this);
        this.handleSignUp = this.handleSignUp.bind(this);
      }
         
      handleChange(event) {
        let input = this.state.input;
        input[event.target.name] = event.target.value;
      
        this.setState({
          input
        });
      }
         
      handleSignUp(event) {

        event.preventDefault();
      
        if(this.validate()){
            console.log(this.state);
      
            let input = {};
            input["id" ]="this.props.match.params.id";
            input["first_name"] = "";
            input["last_name"] = "";
            input["email"] = "";
            input["password"] = "";
            this.setState({input:input});
            alert('Registration Successfull');
        
        }

        const  input= this.state.input;
        axios.post(`${backendUrl}/api/v1/register/`,input)
        .then( (response) => {
            console.log(response);
            this.setState ({status:response.data.input.status});
            
        });
      }
      
      validate(){
          let input = this.state.input;
          let errors = {};
          let isValid = true;
       
          if (!input["first_name"]) {
            isValid = false;
            errors["first_name"] = "Please enter your first_name.";
          }
            if (!input["last_name"]) {
                isValid = false;
                errors["last_name"] = "Please enter your last_name.";
          }
      
         
      
          if (!input["email"]) {
            isValid = false;
            errors["email"] = "Please enter your email Address.";
          }
      
          if (typeof input["email"] !== "undefined") {
              
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(input["email"])) {
              isValid = false;
              errors["email"] = "Please enter valid email address.";
            }
          }
      
          if (!input["password"]) {
            isValid = false;
            errors["password"] = "Please enter your password.";
          }
      
         
      
          if (typeof input["password"] !== "undefined") {
            if(input["password"].length < 6){
                isValid = false;
                errors["password"] = "Please add at least 6 charachter.";
            }
          }
         
          
         
      
          this.setState({
            errors: errors
          });
      
          return isValid;
      }

      // componentDidMount(){
       

      //   if(this.state.input === '_add'){
      //     return
      // }else{
      //     SportsService.registerPlayer(this.state.input).then( (res) =>{
      //         let input = res.data;
      //         this.setState({input
      //         });
      //     });
      // } 
      // }

      
    
    render() {
        return (

          <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <Link className="navbar-brand" to={"/home"}>Sports_Management_System</Link>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to={"/sign-in"}>Admin-Login</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/psign-in"}>Player-Login</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="auth-wrapper">
        <div className="auth-inner">
                <div>
        <h3>Sign Up</h3>
        <form onSubmit={this.handleSignUp}>
  
          <div class="form-group">
            <label for="first_name">First Name:</label>
            <input 
              type="text" 
              name="first_name" 
              value={this.state.input.first_name}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter first_name" 
              id="first_name" />
  
              <div className="text-danger">{this.state.errors.first_name}</div>
          </div>

          <div class="form-group">
            <label for="last_name">Last Name:</label>
            <input 
              type="text" 
              name="last_name" 
              value={this.state.input.last_name}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter last_name" 
              id="last_name" />
  
              <div className="text-danger">{this.state.errors.last_name}</div>
          </div>

          <div class="form-group">
            <label for="email">Email Address:</label>
            <input 
              type="text" 
              name="email" 
              value={this.state.input.email}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter email" 
              id="email" />
  
              <div className="text-danger">{this.state.errors.email}</div>
          </div>
  
          <div class="form-group">
            <label for="password">Password:</label>
            <input 
              type="password" 
              name="password" 
              value={this.state.input.password}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter password" 
              id="password" />
  
              <div className="text-danger">{this.state.errors.password}</div>
          </div>
          <input type="submit" value="SignUp" class="btn btn-success" />
        </form>
      </div>
      </div>
      </div>
      </div>
    );
  }

  
}
