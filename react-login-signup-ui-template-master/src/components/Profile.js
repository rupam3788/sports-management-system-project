import React, { Component } from 'react';
import HeaderProfile from './HeaderProfile';
import FooterProfile from './FooterProfile';
import SportsService from '../services/SportsService';


class Profile extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            player: {},
            playerId:""
        }
        this.update = this.update.bind(this);
    }

    update(playerId){
        if(this.state.playerId!=null){
        this.props.history.push(`/update-player/${this.state.playerId}`);
        }
    }

    componentDidMount(){
        if(localStorage.getItem("pid")!=null){
        console.log(localStorage.getItem("pid"))

        const  input= localStorage.getItem("pid");

        SportsService.getPlayerDetailsById(input).then( res => {
            this.setState({player: res.data});//update button
            this.setState({playerId:input})
            console.log(this.state.playerId)
            console.log(res.data);
        })
    }
    }
    

    render() {
        return (
            <div>
                <HeaderProfile/>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> Your Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> Father's Name: </label>
                            <div> { this.state.player.fathername }</div>
                        </div>
                        <div className = "row">
                            <label> Date of Birth: </label>
                            <div> { this.state.player.dob }</div>
                        </div>
                        <div className = "row">
                            <label> Gender: </label>
                            <div> { this.state.player.gender }</div>
                        </div>
                        <div className = "row">
                            <label> Address: </label>
                            <div> { this.state.player.address }</div>
                        </div>
                        <div className = "row">
                            <label> Phone Number: </label>
                            <div> { this.state.player.phone }</div>
                        </div>
                        <div className = "row">
                            <label> College Name: </label>
                            <div> { this.state.player.collegename }</div>
                        </div>
                        
                        <button className="btn btn-success" onClick={() => this.update(this.state.playerId)} type="submit" >Update</button>
                       
                    </div>

                </div>
                

                <FooterProfile/>
                
            </div>
        );
    }
}

export default Profile;