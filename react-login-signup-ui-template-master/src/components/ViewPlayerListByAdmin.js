import React, { Component } from 'react';
import SportsService from '../services/SportsService';
import FooterForAdmin from './FooterForAdmin';
import HeaderForAdmin from './HeaderForAdmin';
class ViewPlayerListByAdmin extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            player: {},
            events:[]

        }
    }

    componentDidMount(){
        if(localStorage.getItem("mid")!=null){
        console.log(this.state.id);
        SportsService.getPlayerById(this.state.id).then( res => {
            console.log(res.data)
            this.setState({player: res.data});
            console.log(res.data.events)
            this.setState({events: res.data.events})

        })
    } 
    }
    render() {
        return (
            <div>
                <HeaderForAdmin/>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> Player All Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> Father's Name: </label>
                            <div> { this.state.player.fathername }</div>
                        </div>
                        <div className = "row">
                            <label> Date of Birth: </label>
                            <div> { this.state.player.dob }</div>
                        </div>
                        <div className = "row">
                            <label> Gender: </label>
                            <div> { this.state.player.gender }</div>
                        </div>
                        <div className = "row">
                            <label> Address: </label>
                            <div> { this.state.player.address }</div>
                        </div>
                        <div className = "row">
                            <label> Phone Number: </label>
                            <div> { this.state.player.phone }</div>
                        </div>
                        <div className = "row">
                            <label> College Name: </label>
                            <div> { this.state.player.collegename }</div>
                        </div>
                        <div className = "row">
                            <label> Events: </label>
                            <tbody>
                           {
                               this.state.events.map(
                                event =>
                                   <tr key = {event.id}>
                                        <td> {event.eventname} </td>   
                                
                                   </tr>
                               )
                           }
                       </tbody>
                        </div>

                    
                    </div>

                </div>
                <FooterForAdmin/>
                
            </div>
        );
    }
}
export default ViewPlayerListByAdmin;