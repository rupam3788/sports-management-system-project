import React, { Component } from 'react'
import SportsService from '../services/SportsService';
import HeaderForAdmin from './HeaderForAdmin';
import FooterForAdmin from './FooterForAdmin';

class CreateEventListByAdmin extends Component {
    constructor(props) {
        super(props)

        this.state = {
            managerId: "",
            // event:{eventname:'',date:'',time:''}
            eventname: '',
            date: '',
            time: '',
            
        }
        this.changeEventNameHandler = this.changeEventNameHandler.bind(this);
        this.changeEventDateHandler = this.changeEventDateHandler.bind(this);
        this.changeEventTimeHandler = this.changeEventTimeHandler.bind(this);
        this.saveOrUpdateEvent = this.saveOrUpdateEvent.bind(this);
    }

   
    componentDidMount(){
        this.setState({managerId:localStorage.getItem("mid")})
        // step 4
        // if(this.state.id === '_add'){
        //     return
        // }else{
            // SportsService.getManagerById(this.state.id).then( (res) =>{
            //     let event = res.data;
            //     this.setState({eventname: event.eventname,
            //         date: event.date,
            //         time : event.time
            //     });
            // });
        // }        
    }

    saveOrUpdateEvent= (e) => {
        
        const {eventname,time,managerId, date }=this.state;
        console.log(eventname,time,managerId, date)
        
        let event = {eventname:eventname, date: date, time: time};
        // let manager_id={manager_id:this.state.manager_id};
        console.log('event => ' + JSON.stringify(event));
         console.log('id => ' + JSON.stringify(this.state.id));
        SportsService.addEvent(managerId, event).then( res => {
            console.log(res)
            this.props.history.push('/event');
       });
       e.preventDefault();

    }

    //     SportsService.addEvent(this.state.id,event).then( res => {
    //         this.props.history.push('/event');
    //     });

        
    //     if(this.state.id === '_add'){

    //         SportsService.addEvent(manager_id,event).then(res =>{
    //             this.props.history.push('/event');
    //         });
    //     }else{
    //         SportsService.updateEvent(event, this.state.id).then( res => {
    //             this.props.history.push('/event');
    //         });
    //     }
    // }

        changeEventNameHandler= (event) => {
        this.setState({eventname: event.target.value});
    }

    changeEventDateHandler= (event) => {
        this.setState({date: event.target.value});
    }

    changeEventTimeHandler= (event) => {
        this.setState({time: event.target.value});
    }

    cancel(){
        this.props.history.push('/event');
    }

    // getTitle(){
    //     if(this.state.id === '_add'){
    //         return <h3 className="text-center">Add Event</h3>
    //     }else{
    //         return <h3 className="text-center">Update Event</h3>
    //     }
    // }
    render() {
        return (
            <div>
                <HeaderForAdmin/>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    // this.getTitle()
                                }
                                <div className = "card-body">
                                    <form onSubmit={this.saveOrUpdateEvent}>
                                        <div className = "form-group">
                                            <label> Event Name: </label>
                                            <input placeholder="Event Name" name="eventname" className="form-control" 
                                                // value={this.state.event.eventname} 
                                                onChange={this.changeEventNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Event Date: </label>
                                            <input placeholder="Event Date" name="date" className="form-control" 
                                                // value={this.state.event.date}
                                                 onChange={this.changeEventDateHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Event Time: </label>
                                            <input placeholder="Event Time" name="time" className="form-control" 
                                                // value={this.state.event.time}
                                                 onChange={this.changeEventTimeHandler}/>
                                        </div>

                                        <button className="btn btn-success" type="submit" >Save Event</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
                   <FooterForAdmin/>
            </div>
        )
    }
}

export default CreateEventListByAdmin
