import React, { Component } from 'react';
import FooterForAdmin from './FooterForAdmin';
import HeaderForAdmin from './HeaderForAdmin';
import SportsService from '../services/SportsService';

class UpdateEventByAdmin extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            eventname: '',
            date: '',
            time: ''
        }
        this.changeEventNameHandler = this.changeEventNameHandler.bind(this);
        this.changeEventDateHandler = this.changeEventDateHandler.bind(this);
        this.changeEventTimeHandler = this.changeEventTimeHandler.bind(this);
        this.updateEvent = this.updateEvent.bind(this);
    }

    componentDidMount(){
        SportsService.getEventById(this.state.id).then( (res) =>{
            let event = res.data;
            this.setState({eventname: event.eventname,
                date: event.date,
                time : event.time
            });
        });
    }

    updateEvent = (e) => {
        e.preventDefault();
        let event = {eventname: this.state.eventname, date: this.state.date, time: this.state.time};
        console.log('event => ' + JSON.stringify(event));
        console.log('id => ' + JSON.stringify(this.state.id));
        SportsService.updateEvent(event, this.state.id).then( res => {
            this.props.history.push('/event');
        });
    }
    
    changeEventNameHandler= (event) => {
        this.setState({eventname: event.target.value});
    }

    changeEventDateHandler= (event) => {
        this.setState({date: event.target.value});
    }

    changeEventTimeHandler= (event) => {
        this.setState({time: event.target.value});
    }

    cancel(){
        this.props.history.push('/event');
    }

   
    render() {
        return (
            <div>
                <HeaderForAdmin/>
                 <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                <h3 className="text-center">Update Event</h3>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Event Name: </label>
                                            <input placeholder="Event Name" name="eventname" className="form-control" 
                                                value={this.state.eventname} onChange={this.changeEventNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Event Date: </label>
                                            <input placeholder="Event Date" name="date" className="form-control" 
                                                value={this.state.date} onChange={this.changeEventDateHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Event Time: </label>
                                            <input placeholder="Event Time" name="time" className="form-control" 
                                                value={this.state.time} onChange={this.changeEventTimeHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.updateEvent}>Update Event</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
                <FooterForAdmin/>
            </div>
        );
    }
}

export default UpdateEventByAdmin;