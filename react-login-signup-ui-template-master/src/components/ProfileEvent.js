import React, { Component } from 'react';
import HeaderEventProfile from './HeaderEventProfile';
import FooterEventProfile from './FooterEventProfile';
import SportsService from '../services/SportsService';

class ProfileEvent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            event: [],
            playerId:""
        }
    }

    componentDidMount(){
        if(localStorage.getItem("pid")!=null){
        console.log("hi", localStorage.getItem("pid"))
        this.setState({playerId:localStorage.getItem("pid")})
        console.log(localStorage.getItem("pid"))
        const input = localStorage.getItem("pid");
        console.log("test",input)
        SportsService.getPlayerDetailsById(input).then((res) => {
            console.log(res.data);
            this.setState({ event: res.data.events});
            
        });
    }
    }

    render() {
        return (
            <div>
                <HeaderEventProfile/>
            <h2 className="text-center"> Your Participated Event List</h2>
            
            <br></br>
            <div className = "row">
                   <table className = "table table-striped table-bordered">

                       <thead>
                           <tr>
                               <th className="text-center"> Event Name</th>
                               <th className="text-center"> Event Date</th>
                               <th className="text-center"> Event Time</th>
                               {/* <th> Actions</th> */}
                           </tr>
                       </thead>
                       <tbody>
                           {
                               this.state.event.map(
                                event =>
                                   <tr key = {event.id}>
                                        <td className="text-center"> {event.eventname} </td>   
                                        <td className="text-center"> {event.date}</td>
                                        <td className="text-center"> {event.time}</td>
                                        
                                   </tr>
                               )
                           }
                       </tbody>
                   </table>

            </div>
            <FooterEventProfile/>

       </div>
   );
    }
}


export default ProfileEvent;