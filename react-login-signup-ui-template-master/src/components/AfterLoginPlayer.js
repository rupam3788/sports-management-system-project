import React, { Component } from 'react';
import Header from './Header';
import Footer from './Footer';
import SportsService from '../services/SportsService';
import axios from 'axios';
 //import './App.css';
 import {backendUrl} from './constants';

class AfterLoginPlayer extends Component {
    constructor(props){
        super(props)

        this.state={
            event : [],
            playerId: "",
            // disabled:false
        }

        this.takePartEvent = this.takePartEvent.bind(this);

    }
    takePartEvent(eventid){
        console.log(eventid)
        console.log(localStorage.getItem("pid"))

        const  input= localStorage.getItem("pid");
          //if(eventid)
        axios.put(`${backendUrl}/api/v1/takepartevent/${input}/${eventid}`)
        .then( (response) => {
            console.log(response);
            // this.setState({disabled: true});
            
        });
        

           
    }

    componentDidMount(){
        if(localStorage.getItem("pid")!=null){
        console.log("hi", localStorage.getItem("pid"))
        this.setState({playerId:localStorage.getItem("pid")})
        console.log(localStorage.getItem("pid"))

        SportsService.getEventDetailsinPlayer().then((res) => {
            this.setState({ event: res.data});
        });
    }
    }
    render() {
        return (
            <div>
                <Header/>
            <h2 className="text-center">Event List</h2>
            
            <br></br>
            <div className = "row">
                   <table className = "table table-striped table-bordered">

                       <thead>
                           <tr>
                              
                               <th className="text-center"> Event Name</th>
                               <th className="text-center"> Event Date</th>
                               <th className="text-center"> Event Time</th>
                               <th className="text-center"> Actions</th>
                           </tr>
                       </thead>
                       <tbody>
                           {
                               this.state.event.map(
                                event =>
                                   <tr key = {event.id}>
                                        <td className="text-center"> {event.eventname} </td>   
                                        <td className="text-center"> {event.date}</td>
                                        <td className="text-center"> {event.time}</td>
                                        <td className="text-center">
                                            <button onClick={ () => this.takePartEvent(event.id)}  
                                            className="btn btn-info">Take Part </button>
                                              {/* disabled={this.state.disabled} */}
                                        </td>
                                   </tr>
                               )
                           }
                       </tbody>
                   </table>

            </div>
            <Footer/>

       </div>
   );
    }
}

export default AfterLoginPlayer;