import React, { Component } from "react";
//import SportsService from "../services/SportsService";
 import axios from 'axios';
 //import {backendUrl} from './constants';
 import {  Link } from "react-router-dom";


export default class Playerlogin extends Component {

  // state={
  //   input: {
  //     email:"",
  //     password:"",
  //   },
  //   errors: {}
  // }

    constructor(props) {
        super(props);
        
        this.state = {
          id: this.props.match.params.id,
          email:"",
          password:"",
          input: {
            email:"",
            password:"",
          },
          errors: {}
        };
         
        // this.handleChange = this.handleChange.bind(this);
        // this.handleLogin = this.handleLogin.bind(this);
      }
         
      handleChange=(event) =>{
        let input = this.state.input;
        input[event.target.name] = event.target.value;
        const name = event.target.name;


         //console.log(input)
          
        this.setState({input} )
      }

      
      handleLogin=()=> {
        // event.preventDefault();
        console.log(this.state.input.email)
        axios.get(`http://localhost:7777/api/v1/login/${this.state.input.email}/${this.state.input.password}`)
        // ${this.state.input.email}/${this.state.input.password}
        .then( (response) => {
          console.log("pid",response.data.input.id);
          localStorage.setItem("pid",response.data.input.id )
        if(response.data.status===1){
            this.props.history.push('/afterloginplayerfilldetails');
       }else{
         this.props.history.push('/psign-in');
      }
            
        })
      
        if(this.validate()){
            // console.log(this.state);
      
            let input = {};
            input["email"] = "";
            input["password"] = "";
            this.setState({input:input});

         
     
            // this.handleLogin().then((res)=>{
            //   console.log(res)
            // })
            
              
            
    
        }
        
      }
    
      
      validate(){
          let input = this.state.input;
          let errors = {};
          let isValid = true;
       
         
      
          if (!input["email"]) {
            isValid = false;
            errors["email"] = "Please enter your email Address.";
          }
      
          if (typeof input["email"] !== "undefined") {
              
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(input["email"])) {
              isValid = false;
              errors["email"] = "Please enter valid email address.";
            }
          }
      
          if (!input["password"]) {
            isValid = false;
            errors["password"] = "Please enter your password.";
          }
      
          
      
          if (typeof input["password"] !== "undefined") {
            if(input["password"].length < 6){
                isValid = false;
                errors["password"] = "Please add at least 6 charachter.";
            }
          }
      
          
      
          // this.setState({
          //   errors: errors
          // });
      
          return isValid;
      }

    //   componentDidMount(){
    //     SportsService.loginPlayer().then((res) => {
    //         this.setState({ input: res.data});
    //     });
    // }
    render() {
        return (
        


          
          
          <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <Link className="navbar-brand" to={"/home"}>Sports_Management_System</Link>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to={"/sign-in"}>Admin-Login</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="auth-wrapper">
        <div className="auth-inner">

   
           
        
      
            <div>
            <h3>Player Sign In</h3>

            <div class="form-group">
            <label for="email">Email Address:</label>
            <input 
              type="text" 
              name="email" 
              value={this.state.input.email}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter email" 
              id="email" />
  
              <div className="text-danger">{this.state.errors.email}</div>
          </div>
  
          <div class="form-group">

            <label for="password">Password:</label>
            <input 
              type="password" 
              name="password" 
              value={this.state.input.password}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter password" 
              id="password" />
  
              <div className="text-danger">{this.state.errors.password}</div>
          </div>
          <button onClick={this.handleLogin} class="btn btn-success" >login</button>
    
      </div>
      </div>
    </div>  
    </div>
    
    );
  }


}
                

                
