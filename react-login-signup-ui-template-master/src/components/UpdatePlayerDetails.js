import React, { Component } from 'react';
import SportsService from '../services/SportsService';

class UpdatePlayerDetails extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            fathername: '',
            dob: '',
            gender: '',
            address:'',
            phone:'',
            collegename:'',
            playerId:''
        }
        this.changeFathernameNameHandler = this.changeFathernameNameHandler.bind(this);
        this.changeDateHandler = this.changeDateHandler.bind(this);
        this.changeGenderHandler = this.changeGenderHandler.bind(this);
        this.changeAddressHandler = this.changeAddressHandler.bind(this);
        this.changePhoneHandler = this.changePhoneHandler.bind(this);
        this.changeCollegeHandler = this.changeCollegeHandler.bind(this);
        this.updatePlayer = this.updatePlayer.bind(this);
    }

    componentDidMount(){
        let playerId=localStorage.getItem("pid");
        this.setState({playerId:playerId});
        SportsService.getPlayerById(playerId).then( (res) =>{
            let input = res.data;
            this.setState({fathername: input.fathername,
                dob: input.dob,
                gender : input.gender,
                address:input.address,
                phone:input.phone,
                collegename:input.collegename
            });
        });
    }

    updatePlayer = (e) => {
        e.preventDefault();
        let input = {fathername: this.state.fathername, dob: this.state.dob, gender: this.state.gender,
            address: this.state.address, phone: this.state.phone, collegename: this.state.collegename};
        console.log('event => ' + JSON.stringify(input));
        console.log('id => ' + JSON.stringify(this.state.id));
        SportsService.updatePlayer( this.state.playerId, input).then( res => {
            console.log(res.data)
            this.props.history.push('/profile');
        });
    }
    
    changeFathernameNameHandler= (input) => {
        this.setState({fathername: input.target.value});
    }

    changeDateHandler= (input) => {
        this.setState({dob: input.target.value});
    }

    changeGenderHandler= (input) => {
        this.setState({gender: input.target.value});
    }
    changeAddressHandler= (input) => {
        this.setState({address: input.target.value});
    }

    changePhoneHandler= (input) => {
        this.setState({phone: input.target.value});
    }

    changeCollegeHandler= (input) => {
        this.setState({collegename: input.target.value});
    }

    cancel(){
        this.props.history.push('/profile');
    }

    render() {
        return (
            <div>
                {/* <HeaderForAdmin/>      */}
                 <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                <h3 className="text-center">Update Event</h3>
                                <div className = "card-body">
                                    <form>
                                        <div className = "form-group">
                                            <label> Father's Name  : </label>
                                            <input placeholder="Father's Name" name="fathername" className="form-control" 
                                                value={this.state.fathername} onChange={this.changeFathernameNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Date Of Birth : </label>
                                            <input placeholder="Date Of Birth" name="dob" className="form-control" 
                                                value={this.state.dob} onChange={this.changeDateHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Gender : </label>
                                            <input placeholder="Gender" name="gender" className="form-control" 
                                                value={this.state.gender} onChange={this.changeGenderHandler}/>
                                        </div>

                                        <div className = "form-group">
                                            <label> Address: </label>
                                            <input placeholder="Address" name="address" className="form-control" 
                                                value={this.state.address} onChange={this.changeAddressHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Phone Number : </label>
                                            <input placeholder="Phone Number" name="phone" className="form-control" 
                                                value={this.state.phone} onChange={this.changePhoneHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> college Name: </label>
                                            <input placeholder="College Name" name="collegename" className="form-control" 
                                                value={this.state.collegename} onChange={this.changeCollegeHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.updatePlayer}>Update Player</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
                {/* <FooterForAdmin/> */}
            </div>
        );
    }
}


export default UpdatePlayerDetails;