import React, { Component } from 'react';
import SportsService from '../services/SportsService';
import FooterForAdmin from './FooterForAdmin';
import HeaderForAdmin from './HeaderForAdmin';

class ViewEventListByAdmin extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            event: {}
        }
    }

    componentDidMount(){
        SportsService.getEventById(this.state.id).then( res => {
            this.setState({event: res.data});
        })
    }
    render() {
        return (
            <div>
                <HeaderForAdmin/>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Event Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> Event Name: </label>
                            <div> { this.state.event.eventname }</div>
                        </div>
                        <div className = "row">
                            <label> Event Date: </label>
                            <div> { this.state.event.date }</div>
                        </div>
                        <div className = "row">
                            <label> Event Time: </label>
                            <div> { this.state.event.time }</div>
                        </div>
                    </div>

                </div>
                <FooterForAdmin/>
                
            </div>
        );
    }
}

export default ViewEventListByAdmin;