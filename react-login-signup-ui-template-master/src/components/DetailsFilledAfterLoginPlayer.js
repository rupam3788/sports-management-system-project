import React, { Component } from 'react';
import HeaderForDetails from './HeaderForDetails';
import FooterForDetails from './FooterForDetails';
//import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
//import SportsService from "../services/SportsService";
//import {  Link } from "react-router-dom";
 import axios from 'axios';
 //import './App.css';
 import {backendUrl} from './constants';
//  import SportsService from '../services/SportsService';

class DetailsFilledAfterLoginPlayer extends Component {
    constructor(props) {
        super(props);
        this.state = {
          
          input: {},
          errors: {},
          playerId:""
        };

         
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit= this.handleSubmit.bind(this);
      }
         
      handleChange(event) {
        let input = this.state.input;
        input[event.target.name] = event.target.value;
        // console.log(this.state);
        // console.log(this.state.id);
      
        this.setState({
          input
        });
      }


      componentDidMount(){
        console.log("hi", localStorage.getItem("pid"))
        this.setState({playerId:localStorage.getItem("pid")})
        console.log(localStorage.getItem("pid"))
    }

    //    componentDidMount(){

    //     // step 4
    //     // if(this.state.id === '_add'){
    //     //     return
    //     // }else{
    //         SportsService.getPlayerById(this.state.id).then( (res) =>{
    //             let input = res.data;
    //             this.setState({input:input
    //             });
    //         });
    //     // }        
    // }
         
      handleSubmit(event) {

        event.preventDefault();
      
        if(this.validate()){
            console.log(this.state);
      
            let input = {};
            input["id" ]=localStorage.getItem("pid");
            input["fathername"] = "";
            input["dob"] = "";
            input["gender"] = "";
            input["address"] = "";
            input["phone"] = "";
            input["collegename"] = "";
            this.setState({input:input});
            alert('Details Filled Successfully');
        
        }


        // const {fathername,dob,playerId, gender,address,phone,collegename }=this.state;
        // console.log(fathername,dob,playerId, gender,address,phone,collegename)
        console.log(this.state.input)
         let playerData = this.state.input;

         console.log(playerData);

       // let playerData = {fathername:fathername, dob: dob, gender: gender,
       //   address: address,phone: phone,collegename: collegename};
        //const i=this.state.id;
        axios.post(`${backendUrl}/api/v1/registerplayerdetails/${localStorage.getItem("pid")}`,playerData)
        .then( (response) => {
            console.log(response);
            //  this.setState ({status:response.data.input.status});
            
        });
      }
      
      validate(){
          let input = this.state.input;
          let errors = {};
          let isValid = true;
       
          if (!input["fathername"]) {
            isValid = false;
            errors["first_name"] = "Please enter your fathername.";
          }
            if (!input["dob"]) {
                isValid = false;
                errors["last_name"] = "Please enter your dob.";
          }
      
         
      
          if (!input["gender"]) {
            isValid = false;
            errors["email"] = "Please enter your gender.";
          }
      
        //   if (typeof input["email"] !== "undefined") {
              
        //     var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        //     if (!pattern.test(input["email"])) {
        //       isValid = false;
        //       errors["email"] = "Please enter valid email address.";
        //     }
        //   }
      
          if (!input["address"]) {
            isValid = false;
            errors["password"] = "Please enter your address.";
          }

          if (!input["phone"]) {
            isValid = false;
            errors["password"] = "Please enter your phone.";
          }
          
          if (!input["collegename"]) {
            isValid = false;
            errors["password"] = "Please enter your collegename.";
          }
      
         
      
        //   if (typeof input["password"] !== "undefined") {
        //     if(input["password"].length < 6){
        //         isValid = false;
        //         errors["password"] = "Please add at least 6 charachter.";
        //     }
        //   }
         
          
         
      
          this.setState({
            errors: errors
          });
      
          return isValid;
      }

      // componentDidMount(){
       

      //   if(this.state.input === '_add'){
      //     return
      // }else{
      //     SportsService.registerPlayer(this.state.input).then( (res) =>{
      //         let input = res.data;
      //         this.setState({input
      //         });
      //     });
      // } 
      // }

      
      render() {
        return (

    //       <div className="App">
    //   <nav className="navbar navbar-expand-lg navbar-light fixed-top">
    //     <div className="container">
    //       <Link className="navbar-brand" to={"/sign-in"}>Sports_Management_System</Link>
    //       <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
    //         <ul className="navbar-nav ml-auto">
    //           <li className="nav-item">
    //             <Link className="nav-link" to={"/sign-in"}>Admin-Login</Link>
    //           </li>
    //           <li className="nav-item">
    //             <Link className="nav-link" to={"/psign-in"}>Player-Login</Link>
    //           </li>
    //           <li className="nav-item">
    //             <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
    //           </li>
    //         </ul>
    //       </div>
    //     </div>
    //   </nav>
    <div>
    <HeaderForDetails/>
      <div className="auth-wrapper">
        <div className="auth-inner">
                <div>
        <h3>Kindly Fill Your Details</h3>
        <form onSubmit={this.handleSubmit}>
  
          <div class="form-group">
            <label for="fathername">Father Name:</label>
            <input 
              type="text" 
              name="fathername" 
              value={this.state.input.fathername}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter Father Name" 
              id="fathername" />
  
              <div className="text-danger">{this.state.errors.fathername}</div>
          </div>

          <div class="form-group">
            <label for="dob">Date of Birth:</label>
            <input 
              type="text" 
              name="dob" 
              value={this.state.input.dob}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter Date Of Birth" 
              id="dob" />
  
              <div className="text-danger">{this.state.errors.dob}</div>
          </div>

          <div class="form-group">
            <label for="gender">Gender:</label>
            <input 
              type="text" 
              name="gender" 
              value={this.state.input.gender}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter Gender" 
              id="gender" />
  
              <div className="text-danger">{this.state.errors.gender}</div>
          </div>

          <div class="form-group">
            <label for="address">Address:</label>
            <input 
              type="text" 
              name="address" 
              value={this.state.input.address}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter Your Address" 
              id="address" />
  
              <div className="text-danger">{this.state.errors.address}</div>
          </div>

          <div class="form-group">
            <label for="phone">Phone Number:</label>
            <input 
              type="text" 
              name="phone" 
              value={this.state.input.phone}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter Phone Number" 
              id="phone" />
  
              <div className="text-danger">{this.state.errors.phone}</div>
          </div>
  
          <div class="form-group">
            <label for="collegename">College Name:</label>
            <input 
              type="text" 
              name="collegename" 
              value={this.state.input.collegename}
              onChange={this.handleChange}
              class="form-control" 
              placeholder="Enter College Name" 
              id="collegename" />
  
              <div className="text-danger">{this.state.errors.collegename}</div>
          </div>
          <input type="submit" value="Submit" class="btn btn-success" />
        </form>
      </div>
      </div>
      </div>
      <FooterForDetails/>

      </div>
    );
  }
}

export default DetailsFilledAfterLoginPlayer;





   
    
  

  
